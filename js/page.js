$(document).ready(function() {
    var downloadSegment = $(".download_segment");
    var downloadActivity = $(".download_activity");
    var activityID = "", segmentID = "";


    function hideDownloadButtons() {
        if(downloadSegment.is(':visible'))
            downloadSegment.hide();
        if(downloadActivity.is(':visible'))
            downloadActivity.hide();
    }


    $("#input_gpx_act").keyup(function() {
        hideDownloadButtons();
    });


    downloadActivity.click(function() {
        $("#urlData").remove();
        $("#codeToParse").val('');
        getActivityDetails(activityID);
    });



    downloadSegment.click(function() {
        $("#codeToParse").val('');
        $("#urlData").remove();
        /**
         * First of all we need to extract the real segment id
         *
         * Base url is: http://www.strava.com/segment_efforts/
         */
        var url = "http://www.strava.com/activities/" + activityID + "/segments/" + segmentID;
        url = "extract_segment_code.php?segment_url=" + url;

        $.ajax({
            url: url,
            type: 'get',
            success: function(data) {
                var index = data.indexOf('client_data: ') + "client_data: ".length;
                var jsonCode = data.substr(index, data.indexOf('\n', index));
                var jsonMatches = jsonCode.match(/(.*?)(}}}})/);
                var jsonString = jsonMatches[0].toString();
                jsonString = '{"client_data": ' + jsonString;
                jsonString += '}';
                var realJSON = $.parseJSON(jsonString);
                var segmentId = realJSON['client_data']['effortData']['details'][segmentID]['segmentId'];
                if(segmentId == undefined) {
                    alert("Something is wrong! I could not find the segment ID!");
                } else {
                    getSegmentDetails(segmentId);
                }
            }, error: function(data) {
                alert("Something is wrong! An error occurred while extracting the real segment ID!");
                activityID = segmentID = "";
                hideDownloadButtons();
            }
        });
    });



    $(".strava_gpx_res").hide();
    $("#gpxDownloader").hide();
    //Hide the buttons initially
    downloadSegment.hide();
    downloadActivity.hide();
    //Called when the convert to gpx button is clicked
    $("#convert_gpx_button").click(function() {
        //Extract the text from the input tag
        var text = $("#input_gpx_act").val().trim();
        var regex_text = /http:\/\/www.strava.com?\/activities\/?(\d+)\/?((?=segments)segments\/?(\d+)\/*)?/;


        if(text.match(regex_text)) {
            var isSegment = (text.match(regex_text)[3] != undefined);
            activityID = text.match(regex_text)[1];
            downloadActivity.fadeIn(500);
            if(isSegment) {
                downloadSegment.fadeIn(500);
                segmentID = text.match(regex_text)[3];
            }
        } else {
            alert("The URL is invalid! Please check again!");
        }
    });


    /**
     * Function that is responsible for fetching the JSON
     * data from the strava segment
     */
    function getSegmentDetails(segmentid) {
        jQuery('<a/>', {
            id: 'urlData',
            href: 'http://www.strava.com/stream/segments/' + segmentid,
            title: 'GPX Data for ' + segmentid + ' segment',
            text: 'Open this URL and copy/paste the code on the area bellow'
        }).appendTo(".sec_width");
        //Show the area where the user can paste the json code
        $(".strava_gpx_res").show();
    }



    /**
     * Function that is responsible for fetching the
     * JSON data from the strava activity
     */
    function getActivityDetails(stravaId) {
        jQuery('<a/>', {
            id: 'urlData',
            href: 'http://www.strava.com/activities/' + stravaId + '/streams?stream_types[]=latlng&stream_types[]=altitude&stream_types[]=time&stream_types[]=distance&stream_types[]=velocity_smooth',
            title: 'GPX Data for ' + stravaId + ' activity',
            text: 'Open this URL and copy/paste the code on the area bellow'
        }).appendTo(".sec_width");
        //Show the area where the user can paste the json code
        $(".strava_gpx_res").show();
    }




    $("#codeToParse").blur(function() {
        var text = $(this).val();
        var jsonCode = $.parseJSON(text);

        if(jsonCode.latlng.length != jsonCode.altitude.length) {
            alert("Something is wrong, the data doesn't match!");
        } else {
            createXML(jsonCode);
        }
    });



    function createXML(jsonCode) {
        var time = new Date();
        var time_final = new Date();
        var shouldAddTime = !(jsonCode.time == undefined);

        var XML = "";
        XML += '<?xml version="1.0" encoding="UTF-8"?>' + "\n";
        XML += '<gpx xmlns="http://www.topografix.com/GPX/1/1" ' +
            'xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1" xmlns:gpxtrkx="http://www.garmin.com/xmlschemas/TrackStatsExtension/v1" xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3" xmlns:wptx1="http://www.garmin.com/xmlschemas/WaypointExtension/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" creator="Oregon 600" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www8.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackStatsExtension/v1 http://www8.garmin.com/xmlschemas/TrackStatsExtension.xsd http://www.garmin.com/xmlschemas/WaypointExtension/v1 http://www8.garmin.com/xmlschemas/WaypointExtensionv1.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd">' + "\n";
        if(shouldAddTime) {
            XML += "\t" + '<metadata>' + "\n";
            XML += "\t\t" + '<time>' + time.toISOString() + '</time>' + "\n";
            XML += "\t" + '</metadata>' + "\n";
        }
        XML += "\t" + '<trk>' + "\n" +
            "\t\t" + '<name>' + activityID + '</name>' + "\n";
        XML += "\t\t" + '<trkseg>' + "\n";

        for(var i = 0; i < jsonCode.latlng.length; i++) {
            XML += "\t\t\t" + '<trkpt lat="' + jsonCode.latlng[i][0] + '"' + ' lon="' + jsonCode.latlng[i][1] + '">' + "\n";
            XML += "\t\t\t\t" + '<ele>' + jsonCode.altitude[i] + '</ele>' + "\n";
            if(shouldAddTime) {
                time_final = new Date(time.valueOf() + jsonCode.time[i] * 1000);
                XML += "\t\t\t\t" + '<time>' + time_final.toISOString() + '</time>' + "\n";
            }
            XML += "\t\t\t" + '</trkpt>' + "\n";
        }
        XML += "\t\t" + '</trkseg>' + "\n";
        XML += "\t" + '</trk>' + "\n";
        XML += '</gpx>' + "\n";


        //We can now show the data to the users
        $("#gpxDownloader").show();
        createAltitudeChart(jsonCode);
        createDownloadify(XML);
    }


    function createAltitudeChart(jsonCode)
    {
        $("#altitudeChart").remove();

        jQuery('<div/>', {
            id: 'altitudeChart'
        }).appendTo("#charts");


        var points = [];
        var totalDistance = 0;

        //TODO: Change this later
        if(jsonCode.latlng.length > 2)
        {
            for(var i = 1; i < jsonCode.latlng.length; i++) {
                var fPoint = {
                  latitude: jsonCode.latlng[i - 1][0],
                  longitude: jsonCode.latlng[i - 1][1]
                };

                var sPoint = {
                  latitude: jsonCode.latlng[i][0],
                  longitude: jsonCode.latlng[i][1]
                };


                var distance = calculateGPSDistance(fPoint, sPoint);
                var altitude = jsonCode.altitude[i];
                if(altitude < 0) {
                    altitude = 0;
                }
                points.push([totalDistance, altitude]);
                totalDistance += distance;
            }
        }


        var plot = $.jqplot('altitudeChart', [points], {
            title: 'Altitude Chart',
            animate: true,
            seriesDefaults: {
                fill: true
            },
            series:[{
                showMarker: false
            }],
            axes: {
                xaxis: {
                    label: 'KM',
                    pad: 0
                },
                yaxis: {
                    label: 'Altitude'
                }
            }
        });


        var accumulated = calculateAccumulated(jsonCode);
        $("#totalAccum").remove();
        jQuery('<div/>', {
            id: 'totalAccum',
            text: 'Elevation gain: ' + Math.ceil(accumulated) + 'M'
        }).appendTo("#accumulated");


        //After this we want to setup a callback so that when the charts div is clicked the gmaps will be showed
        /**$("#charts").click(function() {
            $("#googleMapsView").remove();
            setupGoogleMaps(jsonCode);
        });*/
    }


    /**function setupGoogleMaps(jsonCode) {
        var gMapsDiv = $('<div/>',
        {
            id: 'googleMapsView',
            css: {
                'width' : '600px'
            }
        });


        var overlayDiv = $('<div/>',
        {
            id: 'mapOverlay',
            css: {
                'width' : '600px',
                'height' : '400px'
            }
        });

        //overlayDiv.appendTo(gMapsDiv);
        //$(".ride_data").append(gMapsDiv).show('slow');
        //buildGoogleMaps(jsonCode);
    }*/



    /**function buildGoogleMaps(jsonCode) {
        var handler = Gmaps.build('Google');
        var jsonData = [];

        for(var i = 0; i < jsonCode.latlng.length; i++)
            jsonData.push({ lat: jsonCode.latlng[i][0], lng: jsonCode.latlng[i][1] });

        handler.buildMap({ internal: { id: 'mapOverlay' }}, function() {
            var lines = handler.addPolylines(
              [
                  jsonData
              ],
              {
                  strokeColor: '#FF0000'
              }
            );

            handler.bounds.extendWith(lines);
            handler.fitMapToBounds();
        });
    }*/



    function calculateAccumulated(jsoncode)
    {
        var accumulated = 0;

        for(var i = 1; i < jsoncode.altitude.length; i++) {
            if(jsoncode.altitude[i - 1] < jsoncode.altitude[i]) {
                accumulated += (jsoncode.altitude[i] - jsoncode.altitude[i - 1]);
            }
        }
        return accumulated;
    }


    //Returns the distance between two GPS points in KM
    function calculateGPSDistance(point1, point2)
    {
        var mi = (3963 * 3.1415926 * Math.sqrt(
            (point2.latitude-point1.latitude) *
                (point2.latitude-point1.latitude) +
                Math.cos(point1.latitude/57.29578) *
                    Math.cos(point1.latitude/57.29578) *
                    (point2.longitude-point1.longitude) *
                    (point2.longitude-point1.longitude)
        ) / 180);

        return mi * 1.609344;
    }


    function createDownloadify(XML) {
        //Scroll to top so the user is able to see the "Save to disk" button
        $("html, body").animate({
            scrollTop: $(".strava_input_sec").offset().top
        }, 2000);


        Downloadify.create('gpxDownloader', {
            filename: function() {
                return activityID + ".gpx";
            },

            data: function() {
                return XML;
            },

            onComplete: function(){
                alert('The GPX file was saved to the computer!');
            },

            onCancel: function(){

            },

            onError: function(){
                alert('Something went wrong while saving the file!');
            },

            transparent: false,
            downloadImage: "images/download.png",
            swf: "media/downloadify.swf",
            width: 100,
            height: 30,
            transparent: true,
            append: false
        });
    }
});