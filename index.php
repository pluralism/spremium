<!DOCTYPE html>


<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="content-language" content="en" />
        <meta name="title" content="Strava GPX downloader" />
        <meta name="description" content="Download activities or segments from Strava" />
        <title>Strava GPX downloader</title>


        <link rel="stylesheet" type="text/css" href="css/page.css.css" />


        <script type="application/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script type="application/javascript" src="js/jquery.jqplot.min.js"></script>
        <script type="application/javascript" src="js/swfobject.js"></script>
        <script type="application/javascript" src="js/downloadify.min.js"></script>
        <script type="application/javascript" src="js/page.js"></script>


        <script type="text/javascript">
            var _paq = _paq || [];
            _paq.push(['trackPageView']);
            _paq.push(['enableLinkTracking']);
            (function() {
                var u="//piwik-pluralism.rhcloud.com/";
                _paq.push(['setTrackerUrl', u+'piwik.php']);
                _paq.push(['setSiteId', 1]);
                var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
                g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
            })();
        </script>
        <noscript><p><img src="//piwik-pluralism.rhcloud.com/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>
    </head>




    <body>
        <div id="content">
            <div id="strava_content" class="strava_main_content">
                <div id="strava_description" class="strava_main_content">
                    <div class="strava_intro">
                        <div class="strava_header">
                            <div class="big_text">
                                This is a <a href="http://www.strava.com/">Strava</a> GPX Downloader
                            </div>

                            <div class="small_text">
                                If you like Strava please support the developers and sign up for a premium account!
                            </div>
                        </div>
                    </div>

                    <div class="strava_input_sec">
                        <div class="sec_width">
                            <div class="ride_data">
                                <div id="charts">
                                </div>


                                <div id="accumulated">
                                </div>
                            </div>

                            <input id="input_gpx_act" placeholder="Strava Activity URL" type="text" />
                            <div id="header_buttons">
                                <ul class="download-buttons">
                                    <li class="header_button download_segment">
                                        <button type="button" class="btn btn-danger" id="download_segment">Download segment</button>
                                    </li>

                                    <li class="header_button download_activity">
                                        <button type="button" class="btn btn-info" id="download_activity">Download activity</button>
                                    </li>
                                </ul>
                            </div>
                            <button type="button" class="btn btn-success" id="convert_gpx_button">Convert to GPX!</button>
                            <br />
                        </div>
                    </div>
                </div>

                <p id="gpxDownloader">You must have Flash Player installed!</p>
                <div class="strava_gpx_res">
                    <textarea id="codeToParse" rows="30" cols="30"></textarea>
                </div>
            </div>
        </div>
    </body>
</html>